const express = require("express");
const router = express.Router();
const Account = require("../models/Account");
const connect = require("../dbconnect");
const { PredictionServiceClient } = require('@google-cloud/automl').v1;
const model = {
  projectId: "bernhackt-287619",
  location: "us-central1",
  modelId: "ICN4178375082990632960"
};

router.route("/").post((req, res, next) => {
  console.log("Entering Method: POST pintRoute");
  res.setHeader("Content-Type", "application/json");
  const client = new PredictionServiceClient({ keyFilename: "./auth/BernHackt-c25f235ad887.json" });

  let data = req.body.image;

  let buff = new Buffer(data, 'base64');
  const request = {
    name: client.modelPath(model.projectId, model.location, model.modelId),
    payload: {
      image: {
        imageBytes: buff
      },
    },
    params: {
      score_threshold: '0.8',
    }
  };

  client.predict(request).then(response => {
    console.log(response[0].payload);

    let matchArrayVisionLike = [{
      description: response[0].payload[0].displayName,
      score: response[0].payload[0].classification.score
    }];
    console.log(matchArrayVisionLike);
    if (matchArrayVisionLike[0].description === "CocaCola" && matchArrayVisionLike[0].score > 0.999) {
      console.log("CocaCola Matched");
      // When Coke was recognised, we say OK
      req.app.socket.emit('bottle inserted', { bottle: 1, matches: matchArrayVisionLike });
      res.statusCode = 200;
      let userID = "zGingi";
      connect.then(db => {
        let data = Account.findOne({ UserID: userID }).then(account => {
          console.log(account);
          //save Account Count to the database
          if (account && account.Counter) {
            account.Counter += parseInt(1);
            account.save();
          } else {
            account = { UserID: userID, Counter: 1 };
            let newAccount = new Account(account);
            newAccount.save();
          }
        });
      });
      res.send({ successful: true });
    } else {
      req.app.socket.emit('bottle inserted', { bottle: 0, matches: [{ description: "Objekt nicht erkannt", score: 0 }] });
      res.statusCode = 418;
      res.send({ successful: true });
    }

  }).catch(err => {
    console.error(err);
  });

});

module.exports = router;
