package com.example.pint.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;
/*
Shared Preferences are stored as a file in the filesystem on the device. They are, by default, stored within the app's data directory with
filesystem premissions set that only allow the UID that the specific application runs with to access them. So, they are private in so much
as Linux file permissions restrict access to them, the same as on any Linux/Unix system.

Anyone with root level access to the device will be able to see them, as root has access to everything on the filesystem. Also, any application
that runs with the same UID as the creating app would be able to access them (this is not usually done and you need to take specific action to
make two apps runs with the same UID, so this is probably not a big concern). Finally, if someone was able to mount your device's filesystem
without using the installed Android OS, they could also bypass the permissions that restrict access.

Source: https://stackoverflow.com/questions/9244318/android-sharedpreference-security
 */

public class SharedPreferenceHelper {

    //Source: https://github.com/nickescobedo/Android-Shared-Preferences-Helper/blob/master/SharedPreferenceHelper.java

    private final static String PREF_FILE = "PREF_PINT";

    /**
     * Set a string shared preference
     * @param context - Context (Activity that is calling the function)
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    public static void setSharedPreferenceString(Context context, String key, String value){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();

        new DBLogWriter().info(context, "setSharedPreferenceString() Key = '" + key +"' | Value = '"+value+"'");
    }

    /**
     * Set a integer shared preference
     * @param context - Context (Activity that is calling the function)
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    public static void setSharedPreferenceInt(Context context, String key, int value){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();

        new DBLogWriter().info(context, "setSharedPreferenceInt() Key = '" + key +"' | Value = '"+value+"'");
    }

    /**
     * Set a Boolean shared preference
     * @param context - Context (Activity that is calling the function)
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    public static void setSharedPreferenceBoolean(Context context, String key, boolean value){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.apply();

        new DBLogWriter().info(context, "setSharedPreferenceBoolean() Key = '" + key +"' | Value = '"+value+"'");
    }

    /**
     * Get a string shared preference
     * @param context - Context (Activity that is calling the function)
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    public static String getSharedPreferenceString(Context context, String key, String defValue){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        return settings.getString(key, defValue);
    }

    /**
     * Get a integer shared preference
     * @param context - Context (Activity that is calling the function)
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    public static int getSharedPreferenceInt(Context context, String key, int defValue){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        return settings.getInt(key, defValue);
    }

    /**
     * Get a boolean shared preference
     * @param context - Context (Activity that is calling the function)
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    public static boolean getSharedPreferenceBoolean(Context context, String key, boolean defValue){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        return settings.getBoolean(key, defValue);
    }


    /**
     * Get All Shared Preferneces in a Map
     * @param context - Context (Activity that is calling the function)
     * @return Map<String, ?> - All SharedPreferences
     */
    public static Map<String, ?> getAllSharedPreferences(Context context){
        return context.getSharedPreferences(PREF_FILE, 0).getAll();
    }

    public static void deletePreference(Context context, String key){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        settings.edit().remove(key).apply();
        new DBLogWriter().info(context, "deletePreference() Key = '" + key +"'");
    }

    public static void resetPreferences(Context context){
        SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
        settings.edit().clear().apply();
        new DBLogWriter().info(context, "resetPreferences()");
    }
}