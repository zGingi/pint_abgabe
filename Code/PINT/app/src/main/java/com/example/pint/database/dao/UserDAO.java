package com.example.pint.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.pint.database.entities.User;

import java.util.List;

@Dao
public interface UserDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User... user);

    @Query("SELECT * FROM User")
    List<User> getAllUser();

    @Update
    void updateUser(User... user);

    @Delete
    void deleteUser(User... user);

    @Query("SELECT * FROM User WHERE user_code = :search")
    User findUserWithCode(String search);

}
