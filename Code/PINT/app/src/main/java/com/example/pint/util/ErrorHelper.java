package com.example.pint.util;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Looper;

import com.example.pint.R;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.ErrorCodesEnum;
import com.example.pint.enums.SharedPreferencesEnum;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class ErrorHelper {

    public void showErrorAlert(final Context ctx, final String errorContent, final Boolean removeUncaugghtExceptionFlag) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                builder.setTitle(ctx.getResources().getString(R.string.fehler_aufgetreten))
                        .setMessage(ctx.getResources().getString(R.string.fehler_body) + "\n\n******************\nError Content for Support:\n\n" + errorContent + "\n******************")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setCancelable(false)
                        .setPositiveButton(ctx.getResources().getString(R.string.weiter_upper_case), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                if(removeUncaugghtExceptionFlag) {
                                    //Reset Flag for error:
                                    SharedPreferenceHelper.setSharedPreferenceInt(ctx, SharedPreferencesEnum.UNCAUGHT_EXCEPTION_OCCURRED.toString(), 0);
                                }
                                dialog.dismiss();
                            }
                        }).show();
                Looper.loop();
            }
        }.start();
    }

    public void showErrorAlert_Extended(final Context ctx, final String errorMessage, final String errorTitel, final String buttonDismiss) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        new Thread() {
            @Override
            public void run() {
            Looper.prepare();
            builder.setTitle(errorTitel)
                .setMessage(errorMessage)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(buttonDismiss, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }).show();
            Looper.loop();
            }
        }.start();
    }

    public void showErrorAlert_Exception(final Context ctx, final Exception e) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();

                Writer writer = new StringWriter();
                e.printStackTrace(new PrintWriter(writer));
                String errorContent = writer.toString();

                builder.setTitle(ctx.getResources().getString(R.string.fehler_aufgetreten))
                        .setMessage(ctx.getResources().getString(R.string.fehler_body) + "\n\n******************\nError Content for Support:\n\n" + errorContent + "\n******************")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(ctx.getResources().getString(R.string.weiter_upper_case), new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();
                            }
                        }).show();
                Looper.loop();
            }
        }.start();
    }

    public void showErrorAlert_Exception(final Context ctx, final Exception e, final ErrorCodesEnum errorCodesEnum, final String logText, final boolean displayErrorWindow, final boolean doRestart) {

        if(displayErrorWindow) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            new Thread() {
                @Override
                public void run() {
                    Looper.prepare();

                    String errorContent = logText;

                    if (e != null) {
                        Writer writer = new StringWriter();
                        e.printStackTrace(new PrintWriter(writer));
                        errorContent += "\n\nException Stack Trace:\n\n" + writer.toString();
                    }

                    builder.setTitle(ctx.getResources().getString(R.string.fehler_aufgetreten))
                            .setMessage("Fehler \"" + errorCodesEnum.getEnumString() + "\"\n\n" + ctx.getResources().getString(R.string.fehler_body) + "\n\n******************\nError Content for Support:\n\n" + errorContent + "\n******************")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(ctx.getResources().getString(R.string.weiter_upper_case), new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if (doRestart) {
                                        doRestart(ctx);
                                    } else {
                                        dialog.dismiss();
                                    }
                                }
                            }).show();
                    Looper.loop();
                }
            }.start();
        }else if (doRestart){
            doRestart(ctx);
        }
    }

    public void showErrorAlert_Extended(final Context ctx, final String errorContent, final String errorMessage, final String errorTitel, final String buttonDismiss) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                builder.setTitle(errorTitel)
                    .setMessage(errorMessage + "\n\n******************\nError Content for support:\n\n" + errorContent + "\n******************")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(buttonDismiss, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    }).show();
                Looper.loop();
            }
        }.start();
    }

    public void showErrorAlert_Extended(final Context ctx, final StringBuilder errorContent, final String errorMessage, final String errorTitel, final String buttonDismiss) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        new Thread() {
            @Override
            public void run() {
            Looper.prepare();
            builder.setTitle(errorTitel)
                .setMessage(errorMessage + "\n\n******************\nError Content for support:\n\n" + errorContent + "\n******************")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(buttonDismiss, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }).show();
            Looper.loop();
            }
        }.start();
    }

    //Restart the App
    //Source: https://stackoverflow.com/questions/6609414/how-do-i-programmatically-restart-an-android-app
    public static void doRestart(Context c) {

        DBLogWriter dbLogWriter = new DBLogWriter();

        try {
            //check if the context is given
            if (c != null) {
                //fetch the packagemanager so we can get the default launch activity
                // (you can replace this intent with any other activity if you want
                PackageManager pm = c.getPackageManager();
                //check if we got the PackageManager
                if (pm != null) {
                    //create the intent with the default start activity for your application
                    Intent mStartActivity = pm.getLaunchIntentForPackage(
                            c.getPackageName()
                    );
                    if (mStartActivity != null) {
                        mStartActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //create a pending intent so the application is restarted after System.exit(0) was called.
                        // We use an AlarmManager to call this intent in 100ms
                        int mPendingIntentId = 223344;
                        PendingIntent mPendingIntent = PendingIntent
                                .getActivity(c, mPendingIntentId, mStartActivity,
                                        PendingIntent.FLAG_CANCEL_CURRENT);
                        AlarmManager mgr = (AlarmManager) c.getSystemService(Context.ALARM_SERVICE);
                        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
                        //kill the application
                        System.exit(0);
                    } else {
                        dbLogWriter.error(c, null, "Was not able to restart application, mStartActivity null" , ErrorCodesEnum.ERROR_90008, false);
                    }
                } else {
                    dbLogWriter.error(c, null,"Was not able to restart application, PM null", ErrorCodesEnum.ERROR_90008, false);
                }
            } else {
                dbLogWriter.error(c, null, "Was not able to restart application, Context null", ErrorCodesEnum.ERROR_90008, false);
            }
        } catch (Exception ex) {
            dbLogWriter.error(c, null, "Was not able to restart application", ErrorCodesEnum.ERROR_90008, false);
        }
    }

}
