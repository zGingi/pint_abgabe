package com.example.pint;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.navigation.NavigationView;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.DBLogWriter;
import com.example.pint.util.LanguageHelper;
import com.example.pint.util.LogoutHelper;
import com.example.pint.util.SharedPreferenceHelper;
import com.example.pint.util.UnCaughtException;
import com.example.pint.util.ViewUtils;

import java.util.List;
import java.util.Locale;

/**
 * Created by ima02 on 19.07.2017.
 */

public class EinstellungenActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    String TAG = EinstellungenActivity.class.getName();

    TextView aktivesHausTextView;
    String activeHaus;
    String activeMitarbeiter;

    List<String> stammdatenNamen;

    SettingsModel settingsModel;

    int activeProfile = 0;

    RadioGroup langGroup;

    RadioButton lang_de;
    RadioButton lang_fr;
    RadioButton lang_it;

    DBLogWriter dBLogWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.updateLanguage(EinstellungenActivity.this);


        if (!ViewUtils.isLandscapeEnabled(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.activity_einstellungen);

        dBLogWriter = new DBLogWriter();
        dBLogWriter.info(EinstellungenActivity.this, "////////////////////////////////////////////////////");
        dBLogWriter.info(EinstellungenActivity.this, "//STARTET EINSTELLUNGEN ACTIVITY");
        dBLogWriter.info(EinstellungenActivity.this, "//Source Call: " + getIntent().getStringExtra("Source"));
        dBLogWriter.info(EinstellungenActivity.this, "////////////////////////////////////////////////////");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(EinstellungenActivity.this));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        settingsModel = ViewModelProviders.of(EinstellungenActivity.this).get(SettingsModel.class);


        //*****************************
        // ACTIVE HAUS
        //*****************************

        aktivesHausTextView = (TextView) findViewById(R.id.aktives_haus_titel_favoriten);

        // *****************************
        // LANGUAGE
        //*****************************

        lang_de = (RadioButton) findViewById(R.id.radiobutton_lang_de);
        lang_fr = (RadioButton) findViewById(R.id.radiobutton_lang_fr);
        lang_it = (RadioButton) findViewById(R.id.radiobutton_lang_it);

        switch (SharedPreferenceHelper.getSharedPreferenceInt(EinstellungenActivity.this, SharedPreferencesEnum.ACTIVE_LANGUAGE.toString(), 1)) {
            case 1:
                lang_de.setChecked(true);
                break;
            case 2:
                lang_fr.setChecked(true);
                break;
            case 3:
                lang_it.setChecked(true);
                break;
        }

        langGroup = (RadioGroup) findViewById(R.id.radiogroup_lang);
        langGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, final int checkedId) {

                dBLogWriter.debug(EinstellungenActivity.this, "Checked lang: " + checkedId);

                String languageToLoad = "de";

                if (checkedId == R.id.radiobutton_lang_de) {
                    SharedPreferenceHelper.setSharedPreferenceInt(EinstellungenActivity.this, SharedPreferencesEnum.ACTIVE_LANGUAGE.toString(), 1);
                    languageToLoad = "de";
                } else if (checkedId == R.id.radiobutton_lang_fr) {
                    SharedPreferenceHelper.setSharedPreferenceInt(EinstellungenActivity.this, SharedPreferencesEnum.ACTIVE_LANGUAGE.toString(), 2);
                    languageToLoad = "fr";
                } else if (checkedId == R.id.radiobutton_lang_it) {
                    SharedPreferenceHelper.setSharedPreferenceInt(EinstellungenActivity.this, SharedPreferencesEnum.ACTIVE_LANGUAGE.toString(), 3);
                    languageToLoad = "it";
                }

                Locale locale = new Locale(languageToLoad);
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config,
                        getBaseContext().getResources().getDisplayMetrics());

                new android.app.AlertDialog.Builder(EinstellungenActivity.this)
                        .setTitle(getResources().getString(R.string.titel_sprache_abgeaendert))
                        .setMessage(getResources().getString(R.string.message_sprache_abgeaendert))
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.dismiss();

                                Intent intent = new Intent(EinstellungenActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra("Source", "EinstellungenActivity1");
                                startActivity(intent);
                                finish();
                            }
                        });

            }

        });


        //Hanlde Drawer and navigation
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_einstellungen);
        navigationView.setNavigationItemSelectedListener(this);

        // Show logged in User in Navigation
        View headerView = navigationView.getHeaderView(0);
        TextView loggedInUser = (TextView) headerView.findViewById(R.id.textLoggedInUser);
        loggedInUser.setText(SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_USER.toString(), ""));

        TextView activeModuleTextview = (TextView) headerView.findViewById(R.id.textActiveModule);
        activeModuleTextview.setText(SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_MODULE.toString(), ""));

    }

    @Override
    protected void onStart() {
        super.onStart();
        //Disable Auto-Fill for Oreo
        disableAutoFill();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        //Disable Auto-Fill for Oreo
        //Source: https://stackoverflow.com/questions/45731372/disabling-android-o-auto-fill-service-for-an-application
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(EinstellungenActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("Source", "EinstellungenActivity2");
            startActivity(intent);
            finish();
//            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_erfassung) {

            Intent intent = new Intent(EinstellungenActivity.this, MainActivity.class);
            intent.putExtra("Source", "MainActivity2");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (id == R.id.nav_einstellungen) {

        } else if (id == R.id.nav_info) {

            Intent intent = new Intent(EinstellungenActivity.this, AppInfoActivity.class);
            intent.putExtra("Source", "MainActivity3");
            startActivity(intent);

        } else if (id == R.id.nav_abmelden) {

            //@INFO
            // Starts logout process from LogoutHelper
            LogoutHelper.logout(this, settingsModel);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}