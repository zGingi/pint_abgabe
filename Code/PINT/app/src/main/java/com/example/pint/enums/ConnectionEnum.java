package com.example.pint.enums;

public enum ConnectionEnum {

    //TODO: Encode Users/PW for added security.

    //DEV
    BASIC_AUTH_DEV_USER             ("T_MOBILE"),
    BASIC_AUTH_DEV_PASSWORD         ("Hallo_PINT*123"),

    //TEST
    BASIC_AUTH_TEST_USER             ("T_MOBILE"),
    BASIC_AUTH_TEST_PASSWORD         ("Hallo_PINT*123"),

    //PROD
    BASIC_AUTH_PROD_USER             ("T_MOBILE"),
    BASIC_AUTH_PROD_PASSWORD         ("Hallo_PINT*123");

    private String enumString;

    public String getEnumString() {
        return String.valueOf(enumString);
    }

    ConnectionEnum(String enumString) {
        this.enumString = enumString;
    }

}
