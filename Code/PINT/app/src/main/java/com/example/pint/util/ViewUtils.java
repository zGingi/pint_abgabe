package com.example.pint.util;

import android.content.Context;

import com.example.pint.R;

public class ViewUtils {

    public static boolean isLandscapeEnabled(Context context) {
        return context.getResources().getBoolean(R.bool.isLandscapeEnabled);
    }

}
