package com.example.pint;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.example.pint.barcode.BarcodeCaptureActivity;
import com.example.pint.database.AppDatabase;
import com.example.pint.database.model.UserModel;
import com.example.pint.database.model.log.DBLogModel;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.ErrorCodesEnum;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.DBLogWriter;
import com.example.pint.util.GetFormattedDate;
import com.example.pint.util.LanguageHelper;
import com.example.pint.util.LogoutHelper;
import com.example.pint.util.SharedPreferenceHelper;
import com.example.pint.util.UnCaughtException;
import com.example.pint.util.ViewUtils;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.github.nkzawa.emitter.Emitter;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.net.URISyntaxException;

import pl.droidsonroids.gif.GifImageView;

public class ErfassenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private Socket mSocket;

    {
        try {
            mSocket = IO.socket("https://shrouded-inlet-73857.herokuapp.com/");
        } catch (URISyntaxException e) {
        }
    }

    private static final String TAG = ErfassenActivity.class.getName();

    UserModel userModel;
    SettingsModel settingsModel;
    DBLogModel dbLogModel;

    DBLogWriter dBLogWriter;

    EditText editTextNumber;
    TextView debugView;
    Button abschliessen;

    String activeUser;
    String activeModuleForUser;
    String activeSoundProfileForUser;

    GetFormattedDate getFormattedDate;

//    final MediaPlayer mp_drop =  MediaPlayer.create(this, R.raw.drop_metal);
//    final MediaPlayer mp_cheer =  MediaPlayer.create(this, R.raw.cheer);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LanguageHelper.updateLanguage(ErfassenActivity.this);


        if (!ViewUtils.isLandscapeEnabled(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        dBLogWriter = new DBLogWriter();
        dBLogWriter.info(ErfassenActivity.this, "////////////////////////////////////////////////////");
        dBLogWriter.info(ErfassenActivity.this, "//STARTET ERFASSEN ACTIVITY");
        dBLogWriter.info(ErfassenActivity.this, "//Source Call: " + getIntent().getStringExtra("Source"));
        dBLogWriter.info(ErfassenActivity.this, "////////////////////////////////////////////////////");

        //@Info:    Prevent Keyboard from appearing on activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //@Info:    Content View
        setContentView(R.layout.activity_erfassen);

        //@Info:    Set Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //@Info:    Override Uncaught exception handler
        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(ErfassenActivity.this));

        //Date Helper
        getFormattedDate = new GetFormattedDate();

        //Settings
        activeUser = SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_USER.toString(), "");
        activeModuleForUser = SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_MODULE.toString(), "");
        activeSoundProfileForUser = SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_SOUND_PROFILE.toString(), "");

        //Instantiate Modells
        userModel = ViewModelProviders.of(ErfassenActivity.this).get(UserModel.class);
        settingsModel = ViewModelProviders.of(ErfassenActivity.this).get(SettingsModel.class);
        dbLogModel = ViewModelProviders.of(ErfassenActivity.this).get(DBLogModel.class);

        //Texts
        editTextNumber = (EditText) findViewById(R.id.editTextNumber);
        debugView = (TextView) findViewById(R.id.debug_message);
        debugView.setMovementMethod(new ScrollingMovementMethod());

        //Button
        abschliessen = (Button) findViewById(R.id.button_abschliessen);
        abschliessen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //Connect to Web Socket
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("start session", onNewMessage);
        mSocket.on("bottle inserted", onNewBottel);
        mSocket.connect();

        //@INFO
        // Prevent Drawer from Showing
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

    @Override
    public void onBackPressed() {
        leave();
        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();

        //@INFO: Disable Auto-Fill for Oreo
        disableAutoFill();

        dBLogWriter = new DBLogWriter();
        settingsModel = ViewModelProviders.of(ErfassenActivity.this).get(SettingsModel.class);
        userModel = ViewModelProviders.of(ErfassenActivity.this).get(UserModel.class);

        //TODO: Handlöe auto Login

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        //@INFO:
        //Disable Auto-Fill for Oreo
        //Source: https://stackoverflow.com/questions/45731372/disabling-android-o-auto-fill-service-for-an-application
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }

    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        leave();
        super.onDestroy();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_erfassung) {

            Intent intent = new Intent(ErfassenActivity.this, MainActivity.class);
            intent.putExtra("Source", "ErfassenActivity1");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (id == R.id.nav_einstellungen) {

            Intent intent = new Intent(ErfassenActivity.this, ErfassenActivity.class);
            intent.putExtra("Source", "ErfassenActivity2");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (id == R.id.nav_info) {

            Intent intent = new Intent(ErfassenActivity.this, ErfassenActivity.class);
            intent.putExtra("Source", "ErfassenActivity3");
            startActivity(intent);

        } else if (id == R.id.nav_abmelden) {

            //@INFO
            // Starts logout process from LogoutHelper
            LogoutHelper.logout(this, settingsModel);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    /////////////////////////////////////////
    // WEB SOCKET HANDLING
    //////////////////////////////////////////

    private void attemptNewSession() {

        if (!mSocket.connected()) {
            dBLogWriter.warning(ErfassenActivity.this, "Socket not connected");
            return;
        }

        org.json.JSONObject obj = new org.json.JSONObject();
        try {
            obj.put("UserID", activeUser);
            obj.put("PintID", activeModuleForUser);
            obj.put("SoundID", activeSoundProfileForUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dBLogWriter.summary(ErfassenActivity.this, "Start new Session" + obj.toString());
        debugView.setText(debugView.getText() + "\n" + getFormattedDate.GetFormattedDateTime() + ": Start new Session:");
        debugView.setText(debugView.getText() + "\n\t-User ID: " + activeUser);
        debugView.setText(debugView.getText() + "\n\t-Pint ID: " + activeModuleForUser);
        debugView.setText(debugView.getText() + "\n\t-Sound Profile: " + activeSoundProfileForUser);
        debugView.setText(debugView.getText() + "\n");

        // perform the sending message attempt.
        mSocket.emit("start session", obj);
    }

    private void leave() {

        org.json.JSONObject obj = new org.json.JSONObject();
        try {
            obj.put("UserID", activeUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        dBLogWriter.summary(ErfassenActivity.this, "Leave Session" + obj.toString());
        debugView.setText(debugView.getText() + "\n" + getFormattedDate.GetFormattedDateTime() + ": Leave Session:");

        // perform the sending message attempt.
        mSocket.emit("end session", obj);

        mSocket.off();
        mSocket.disconnect();
    }


    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            ErfassenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dBLogWriter.summary(ErfassenActivity.this, "Socket Connected");
                    debugView.setText(debugView.getText() + "\n" + getFormattedDate.GetFormattedDateTime() + ": Socket connected");

                    attemptNewSession();
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            ErfassenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dBLogWriter.summary(ErfassenActivity.this, "Socket Disconnected");
                    debugView.setText(debugView.getText() + "\n" + getFormattedDate.GetFormattedDateTime() + ": Socket Disconnected");
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            ErfassenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dBLogWriter.error(ErfassenActivity.this, null, "Socket Error", ErrorCodesEnum.ERROR_90001, false);
                    debugView.setText(debugView.getText() + "\n" + getFormattedDate.GetFormattedDateTime() + ": Socket Error");
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            ErfassenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dBLogWriter.summary(ErfassenActivity.this, "New Message: " + args.toString());
                    debugView.setText(debugView.getText() + "\n" + getFormattedDate.GetFormattedDateTime() + ": New Message: " + args[0].toString());
                }
            });
        }
    };

    private Emitter.Listener onNewBottel = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            ErfassenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dBLogWriter.summary(ErfassenActivity.this, "New Bottle: " + args[0].toString());
                    debugView.setText(debugView.getText() + "\n" + getFormattedDate.GetFormattedDateTime() + ": New Bottle: " + args[0].toString());

                    //Get response
                    JSONObject jObj = null;
                    try {

                        JSONParser parser = new JSONParser();
                        jObj = (JSONObject) parser.parse(args[0].toString());

                        dBLogWriter.debug(ErfassenActivity.this, "JSON Object: " + jObj.toString());

                        JSONArray msg = (JSONArray) jObj.get("matches");

                        if (msg != null && msg.size() > 0 && jObj.get("bottle").toString().equals("1")) {
                            int count = Integer.parseInt(editTextNumber.getText().toString());
                            count += 1;
                            editTextNumber.setText(count + "");

                            dBLogWriter.debug(ErfassenActivity.this, "First Array Item: " + msg.get(0).toString());

                            jObj = (JSONObject) parser.parse(msg.get(0).toString());

                            dBLogWriter.archive(ErfassenActivity.this, "Identified Object: " +  jObj.get("description").toString());

                            Toast.makeText(ErfassenActivity.this, jObj.get("description").toString(), Toast.LENGTH_SHORT).show();

                            GifImageView gif = findViewById(R.id.gifimage_erfassen);

                            if (count % 5 == 0) {
                                gif.setImageResource(R.drawable.gif_cheers);
                                MediaPlayer mp_cheer = MediaPlayer.create(ErfassenActivity.this, R.raw.cheer);
                                mp_cheer.start();
                            } else {
                                gif.setImageResource(R.drawable.gif_recyclebin3);
                                MediaPlayer mp_drop = MediaPlayer.create(ErfassenActivity.this, R.raw.mario);
                                mp_drop.start();
                            }

                        } else {

                            int count = Integer.parseInt(editTextNumber.getText().toString());
                            count -= 1;
                            editTextNumber.setText(count + "");

                            GifImageView gif = findViewById(R.id.gifimage_erfassen);

                            gif.setImageResource(R.drawable.gif_wrong);
                            MediaPlayer mp_denied = MediaPlayer.create(ErfassenActivity.this, R.raw.denied);
                            mp_denied.start();

                        }

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                }
            });
        }
    };


}