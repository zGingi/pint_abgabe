package com.example.pint;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.ErrorCodesEnum;
import com.example.pint.enums.SettingsEnum;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.DBLogWriter;
import com.example.pint.util.LanguageHelper;
import com.example.pint.util.LogoutHelper;
import com.example.pint.util.SharedPreferenceHelper;
import com.example.pint.util.UnCaughtException;
import com.example.pint.util.ViewUtils;
import com.google.android.material.navigation.NavigationView;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import pl.droidsonroids.gif.GifImageView;


/**
 * Created by ima02 on 19.07.2017.
 */

public class PunkteActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String TAG = PunkteActivity.class.getName();

    SettingsModel settingsModel;

    EditText editTextAccountCount;

    DBLogWriter dbLogWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LanguageHelper.updateLanguage(PunkteActivity.this);

        if (!ViewUtils.isLandscapeEnabled(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //@Info:    Set Content
        setContentView(R.layout.activity_punkte);

        //@Info:    Prevent Keyboard from appearing on activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        dbLogWriter = new DBLogWriter();
        dbLogWriter.info(PunkteActivity.this, "////////////////////////////////////////////////////");
        dbLogWriter.info(PunkteActivity.this, "//STARTET PUNKTE ACTIVITY");
        dbLogWriter.info(PunkteActivity.this, "//Source Call: " + getIntent().getStringExtra("Source"));
        dbLogWriter.info(PunkteActivity.this, "////////////////////////////////////////////////////");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(PunkteActivity.this));

        //Gett settings -> Active user and Active SettingsProfile
        settingsModel = ViewModelProviders.of(PunkteActivity.this).get(SettingsModel.class);


        //EditText
        editTextAccountCount = findViewById(R.id.editTextAccountCount);

        //Get count of total bottles from Heroku Database
        AndroidNetworking.get("https://shrouded-inlet-73857.herokuapp.com/accounts")
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // do anything with response
                        JSONObject jObj = null;
                        try {
                            jObj = response.getJSONObject(0);
                            String counter = jObj.getString("Counter");
                            int count = Integer.parseInt(counter);
                            editTextAccountCount.setText(count+"");
                            GifImageView gif = findViewById(R.id.gifimage_count);

                            if(count <= 5){
                                gif.setImageResource(R.drawable.img_green);
                            }else if (count <= 10){
                                gif.setImageResource(R.drawable.img_blue);
                            }else{
                                gif.setImageResource(R.drawable.img_red);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(ANError e) {
                        // handle error
                        dbLogWriter.error(PunkteActivity.this, e, "Error in GET", ErrorCodesEnum.ERROR_00001, false);
                    }
                });


        //Hanlde Drawer and navigation
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_info);
        navigationView.setNavigationItemSelectedListener(this);

        // Show logged in User in Navigation
        View headerView = navigationView.getHeaderView(0);
        TextView loggedInUser = (TextView) headerView.findViewById(R.id.textLoggedInUser);
        loggedInUser.setText(SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_USER.toString(), ""));

        TextView activeModuleTextview = (TextView) headerView.findViewById(R.id.textActiveModule);
        activeModuleTextview.setText(SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_MODULE.toString(), ""));
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Disable Auto-Fill for Oreo
        disableAutoFill();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        //Disable Auto-Fill for Oreo
        //Source: https://stackoverflow.com/questions/45731372/disabling-android-o-auto-fill-service-for-an-application
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(PunkteActivity.this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("Source", "AppInfoActivity2");
            startActivity(intent);
            finish();
//            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_erfassung) {

            Intent intent = new Intent(PunkteActivity.this, MainActivity.class);
            intent.putExtra("Source", "MainActivity3");
            startActivity(intent);

        } else if (id == R.id.nav_einstellungen) {

            Intent intent = new Intent(PunkteActivity.this, EinstellungenActivity.class);
            intent.putExtra("Source", "MainActivity2");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (id == R.id.nav_info) {

        } else if (id == R.id.nav_abmelden) {

            //@INFO
            // Starts logout process from LogoutHelper
            LogoutHelper.logout(this, settingsModel);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
