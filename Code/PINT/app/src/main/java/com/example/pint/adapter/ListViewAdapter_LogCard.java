package com.example.pint.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;

import  com.example.pint.R;
import  com.example.pint.LogExplorerActivity;
import  com.example.pint.LogOverviewActivity;
import  com.example.pint.database.model.log.DBLogModel;
import  com.example.pint.util.ErrorHelper;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class ListViewAdapter_LogCard extends ArrayAdapter<String[]> implements Filterable {

    String TAG = ListViewAdapter_LogCard.class.getName();

    private List<String[]> items;

    public ListViewAdapter_LogCard(Context context, List<String[]> log) {
        super(context, R.layout.listview_log_card, log);
        this.items = log;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.listview_log_card, null);
        }

        String l[] = getItem(position);

        //Careful Spams the log fil if many Materialen were captured.
//        DBLog.e(TAG, "Trying to show all rows in adapter"+m.toString());

        TextView textview_log_date_as_title = (TextView) v.findViewById(R.id.textview_log_date_as_title);
        TextView textview_log_total = (TextView) v.findViewById(R.id.textview_log_total);

        TextView textview_error_count = (TextView) v.findViewById(R.id.textview_error_count);
        TextView textview_warning_count = (TextView) v.findViewById(R.id.textview_warning_count);
        TextView textview_info_count = (TextView) v.findViewById(R.id.textview_info_count);
        TextView textview_debug_count = (TextView) v.findViewById(R.id.textview_debug_count);
        TextView textview_archive_count = (TextView) v.findViewById(R.id.textview_archive_count);

        View bar_chart_log_error = (View) v.findViewById(R.id.bar_chart_log_error);
        View bar_chart_log_warning = (View) v.findViewById(R.id.bar_chart_log_warning);
        View bar_chart_log_info = (View) v.findViewById(R.id.bar_chart_log_info);
        View bar_chart_log_debug = (View) v.findViewById(R.id.bar_chart_log_debug);
        View bar_chart_log_archive = (View) v.findViewById(R.id.bar_chart_log_archive);

        if (null != l && l.length >= 7) {
            final String datum = l[0];
            int total = Integer.parseInt(l[1]);
            double hundred = 100.0;
            double hundred_dvided_by_total = (hundred/total);
            Log.d("LOG CARD","100/Total = "+hundred_dvided_by_total);
            double totalError = hundred_dvided_by_total * Integer.parseInt(l[2]);
            double totalWarning = hundred_dvided_by_total * Integer.parseInt(l[3]);
            double totalInfo = hundred_dvided_by_total * Integer.parseInt(l[4]);
            double totalDebug = hundred_dvided_by_total * Integer.parseInt(l[5]);
            double totalArchive = hundred_dvided_by_total * Integer.parseInt(l[6]);

            textview_error_count.setText(l[2] + " (" + Math.floor(totalError  * 100) / 100 + "%)");
            textview_warning_count.setText(l[3] + " (" + Math.floor(totalWarning  * 100) / 100 + "%)");
            textview_info_count.setText(l[4] + " (" + Math.floor(totalInfo  * 100) / 100 + "%)");
            textview_debug_count.setText(l[5] + " (" + Math.floor(totalDebug  * 100) / 100 + "%)");
            textview_archive_count.setText(l[6] + " (" + Math.floor(totalArchive  * 100) / 100 + "%)");

            ViewGroup.LayoutParams params = bar_chart_log_error.getLayoutParams();
            params.height = ((int) totalError <= 0)? 1 : (int) totalError * 2;
            bar_chart_log_error.setLayoutParams(params);

            params = bar_chart_log_warning.getLayoutParams();
            params.height = ((int) totalWarning <=0)? 1 : (int) totalWarning * 2;
            bar_chart_log_warning.setLayoutParams(params);

            params = bar_chart_log_info.getLayoutParams();
            params.height = ((int) totalInfo <= 0) ? 1 : (int) totalInfo * 2;
            bar_chart_log_info.setLayoutParams(params);

            params = bar_chart_log_debug.getLayoutParams();
            params.height = ((int) totalDebug <= 0) ? 1 : (int) totalDebug * 2;
            bar_chart_log_debug.setLayoutParams(params);

            params = bar_chart_log_archive.getLayoutParams();
            params.height = ((int) totalArchive <= 0) ? 1 : (int) totalArchive * 2;
            bar_chart_log_archive.setLayoutParams(params);

            textview_log_date_as_title.setText(datum);
            textview_log_total.setText("Total: " + total);

            Button show = (Button) v.findViewById(R.id.button_log_overview_show_log);
            show.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), LogExplorerActivity.class);
                    intent.putExtra("Source", "ListViewAdapter_LogCard1");
                    intent.putExtra("date", datum);
                    getContext().startActivity(intent);
                }
            });
            Button delete = (Button) v.findViewById(R.id.button_log_overview_delete_log);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Log löschen");
                    builder.setMessage("Möchten sie das Log für den Tag " + datum + "wirklich löschen?");

                    // Set up the buttons
                    builder.setPositiveButton("Ja", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                new AsyncTask<Void, Void, Void>() {
                                    @Override
                                    protected Void doInBackground(Void... params) {

                                        DBLogModel dbLogModel = ViewModelProviders.of((FragmentActivity) getContext()).get(DBLogModel.class);
                                        dbLogModel.deleteAllLogsFromDate(datum);

                                        return null;

                                    }

                                    @Override
                                    protected void onPostExecute(Void aVoid) {
                                        Toast.makeText(getContext(), "Log für den Tag " + datum + "wurde gelöscht.", Toast.LENGTH_LONG);
                                        Intent intent = new Intent(getContext(), LogOverviewActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.putExtra("Source", "ListViewAdapter_LogCard");
                                        getContext().startActivity(intent);
                                    }
                                }.execute().get();
                            } catch (InterruptedException | ExecutionException e) {
                               new ErrorHelper().showErrorAlert_Exception(getContext(), e); //e.printStackTrace();
                            }
                        }
                    });
                    builder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                }
            });
        }

        return v;
    }

    public List<String[]> getAllItems() {
        return items;
    }

}

