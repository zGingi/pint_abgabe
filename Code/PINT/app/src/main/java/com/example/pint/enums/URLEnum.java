package com.example.pint.enums;

public enum URLEnum {

    URL_DEV                      (""),
    URL_TEST                     (""),
    URL_PROD                     ("");

    private String enumString;

    public String getEnumString() {
        return String.valueOf(enumString);
    }

    URLEnum(String enumString) {
        this.enumString = enumString;
    }
}
