package com.example.pint;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import com.example.pint.database.AppDatabase;
import com.example.pint.database.entities.User;
import com.example.pint.database.model.UserModel;
import com.example.pint.database.model.log.DBLogModel;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.SettingsEnum;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.DBLogWriter;
import com.example.pint.util.LanguageHelper;
import com.example.pint.util.SharedPreferenceHelper;
import com.example.pint.util.UnCaughtException;
import com.example.pint.util.ViewUtils;
import com.rengwuxian.materialedittext.MaterialEditText;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getName();

    Button login;

    UserModel userModel;
    SettingsModel settingsModel;
    DBLogModel dbLogModel;

    User user;

    EditText username;
    EditText password;

    DBLogWriter dBLogWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LanguageHelper.updateLanguage(LoginActivity.this);


        if (!ViewUtils.isLandscapeEnabled(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        dBLogWriter = new DBLogWriter();
        dBLogWriter.info(LoginActivity.this, "////////////////////////////////////////////////////");
        dBLogWriter.info(LoginActivity.this, "//STARTET LOGIN ACTIVITY");
        dBLogWriter.info(LoginActivity.this, "//Source Call: " + getIntent().getStringExtra("Source"));
        dBLogWriter.info(LoginActivity.this, "////////////////////////////////////////////////////");

        //@Info:    Prevent Keyboard from appearing on activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //@Info:    Content View
        setContentView(R.layout.activity_login);

        //@Info:    Set Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //@Info:    Override Uncaught exception handler
        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(LoginActivity.this));

        //Instantiate Modells
        userModel = ViewModelProviders.of(LoginActivity.this).get(UserModel.class);
        settingsModel = ViewModelProviders.of(LoginActivity.this).get(SettingsModel.class);
        dbLogModel = ViewModelProviders.of(LoginActivity.this).get(DBLogModel.class);

        //@Info:    Initialize EditText
        username = (EditText) findViewById(R.id.username_email);
        password = (EditText) findViewById(R.id.password);

        //@Info:    Initialize Buttons
        login = (Button) findViewById(R.id.sign_in_button);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Because of Prototype, NO Login will be handled here!
                //Therefore we allways execute the "initialSetup()" to set the neccessary Settings
                //for the app to work properly
                initialSetUp();

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.putExtra("Source", "LoginActivity1");
                startActivity(intent);
            }
        });

        //@INFO
        // Prevent Drawer from Showing
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    protected void initialSetUp() {
        SharedPreferenceHelper.setSharedPreferenceString(LoginActivity.this, SharedPreferencesEnum.ACTIVE_USER.toString(), "zGingi");
        SharedPreferenceHelper.setSharedPreferenceString(LoginActivity.this, SharedPreferencesEnum.ACTIVE_MODULE.toString(), "PrototypeBox");
        SharedPreferenceHelper.setSharedPreferenceString(LoginActivity.this, SharedPreferencesEnum.ACTIVE_SOUND_PROFILE.toString(), "Default");
        SharedPreferenceHelper.setSharedPreferenceInt(LoginActivity.this, SharedPreferencesEnum.ACTIVE_ENVIROMENT.toString(), SettingsEnum.ACTIVE_ENVIROMENT.getEnumInt());
        SharedPreferenceHelper.setSharedPreferenceInt(LoginActivity.this, SharedPreferencesEnum.IS_DEBUG_ENABLED.toString(), SettingsEnum.IS_DEBUG_ENABLED.getEnumInt());
    }

    @Override
    protected void onStart() {
        super.onStart();

        //@INFO: Disable Auto-Fill for Oreo
        disableAutoFill();

        dBLogWriter = new DBLogWriter();
        settingsModel = ViewModelProviders.of(LoginActivity.this).get(SettingsModel.class);
        userModel = ViewModelProviders.of(LoginActivity.this).get(UserModel.class);

        //TODO: Handlöe auto Login

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        //@INFO:
        //Disable Auto-Fill for Oreo
        //Source: https://stackoverflow.com/questions/45731372/disabling-android-o-auto-fill-service-for-an-application
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }

    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        //@INFO
        //Ask User to exit App
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle(getResources().getString(R.string.app_schliessen).toUpperCase())
                .setMessage(getResources().getString(R.string.app_wirklich_schliessen) + "?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(R.string.schliessen_upper_case, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        dBLogWriter.info(LoginActivity.this, "Trying to exit the app");
                        finishAndRemoveTask();
                    }
                }).setNegativeButton(R.string.abbrechen, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}