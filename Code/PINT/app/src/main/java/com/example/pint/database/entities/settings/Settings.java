package com.example.pint.database.entities.settings;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "settings")
public class Settings {

    @PrimaryKey(autoGenerate = true)
    Integer id;

    @ColumnInfo(name = "active_user")
    String active_user;

    @ColumnInfo(name = "is_debug_modus_active")
    Integer is_debug_modus_active;

    //Set enviroment with this variable
    // 1 = DEV
    // 2 = TEST
    // 3 = PROD
    @ColumnInfo(name = "enviroment")
    Integer enviroment;


    public Settings(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActive_user() {
        return active_user;
    }

    public void setActive_user(String active_user) {
        this.active_user = active_user;
    }

    public Integer getIs_debug_modus_active() {
        return is_debug_modus_active;
    }

    public void setIs_debug_modus_active(Integer is_debug_modus_active) {
        this.is_debug_modus_active = is_debug_modus_active;
    }

    public Integer getEnviroment() {
        return enviroment;
    }

    public void setEnviroment(Integer enviroment) {
        this.enviroment = enviroment;
    }


    @Override
    public String toString() {
        return "Settings{" +
                "id=" + id +
                ", active_user='" + active_user + '\'' +
                ", is_debug_modus_active=" + is_debug_modus_active +
                ", enviroment=" + enviroment +
                '}';
    }
}