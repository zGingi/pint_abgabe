package com.example.pint.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Base64;

import com.example.pint.enums.ConnectionEnum;
import com.example.pint.enums.ErrorCodesEnum;
import com.example.pint.enums.URLEnum;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;

//Source:
//https://stackoverflow.com/questions/6493517/detect-if-android-device-has-internet-connection
public class ConnectionHelper {

    static final String TAG = ConnectionHelper.class.getName();

    static String responseCode = "";

    static DBLogWriter dbLogWriter;

    public static boolean isNetworkAvailable(final Context context) {

        dbLogWriter = new DBLogWriter();

        try {
            return new AsyncTask<Void, Void, Boolean>() {
                @Override
                protected Boolean doInBackground(Void... params) {
                    ConnectivityManager connectivityManager
                            = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

                    if(activeNetworkInfo != null){
                        dbLogWriter.info(context, "isNetworkAvailable = TRUE");
                    }else{
                        dbLogWriter.warning(context, "isNetworkAvailable = FALSE");
                    }
                    return activeNetworkInfo != null;
                }
            }.execute().get();
        } catch (InterruptedException | ExecutionException e) {
            dbLogWriter.error(context, e,"InterruptedException | ExecutionException: " + Arrays.toString(e.getStackTrace()), ErrorCodesEnum.ERROR_90004, false);
        }
        dbLogWriter.warning(context, "isNetworkAvailable = FALSE");
        return false;

    }

    @SuppressLint("StaticFieldLeak")
    public static boolean hasConnectionToBackend(final Context context, final int env) {

        dbLogWriter = new DBLogWriter();

        try {
            return new AsyncTask<Void, Void, Boolean>() {

                @Override
                protected Boolean doInBackground(Void... params) {
                    try {

                        //@DEBUG
                        dbLogWriter.info(context, "Try to connect: env: " + env);

                        String authorization = getUserAndPassword(env);
                        String url = getURL(env);

                        //@DEBUG
                        dbLogWriter.info(context, "Try to connect: url: " + url);
                        //@DEBUG
                        dbLogWriter.debug(context, "Try to connect: Authorization: " + authorization);
                        //@DEBUG
                        dbLogWriter.debug(context, "Try to connect: pw: ***********");

                        URL localUrl = new URL(url);

                        if (url.contains("https")) {
                            dbLogWriter.info(context, "Use HTTPS connection");

                            HttpsURLConnection connection = (HttpsURLConnection) localUrl.openConnection();

                            dbLogWriter.debug(context, "Set Request properties");
                            connection.setRequestProperty("Authorization", "Basic " + authorization);
                            connection.setRequestProperty("accept", "application/json");
                            connection.setRequestMethod("GET");
                            connection.setConnectTimeout(1500);

                            //@DEBUG
                            dbLogWriter.debug(context, "Connection response Code: " + connection.getResponseCode() + " " + connection.getResponseMessage());
                            Map allHeaders = connection.getHeaderFields();
                            dbLogWriter.debug(context, "Connection All headers: " + allHeaders.toString());

                            setResponseCode(connection.getResponseCode() + " " + connection.getResponseMessage());

                            if(connection.getResponseCode() == 200){
                                dbLogWriter.info(context, "isConnectionToBackendAvailable = TRUE (" + connection.getResponseCode() + " - " + connection.getResponseMessage() + ")");
                            }else{
                                dbLogWriter.warning(context, "isConnectionToBackendAvailable = FALSE (" + connection.getResponseCode() + " - " + connection.getResponseMessage() + ")");
                            }

                            return (connection.getResponseCode() == 200);

                        } else {
                            dbLogWriter.info(context, "Use HTTP connection");
                            HttpURLConnection connection = (HttpURLConnection) localUrl.openConnection();
                            connection.setRequestProperty("Authorization", "Basic " + authorization);
                            connection.setRequestProperty("accept", "application/json");
                            connection.setRequestMethod("GET");
                            connection.setConnectTimeout(1500);

                            //@DEBUG
                            dbLogWriter.debug(context, "Connection response Code: " + connection.getResponseCode() + " " + connection.getResponseMessage());
                            Map allHeaders = connection.getHeaderFields();
                            dbLogWriter.debug(context, "Connection All headers: " + allHeaders.toString());

                            setResponseCode(connection.getResponseCode() + " " + connection.getResponseMessage());

                            if(connection.getResponseCode() == 200){
                                dbLogWriter.info(context, "isConnectionToBackendAvailable = TRUE (" + connection.getResponseCode() + " - " + connection.getResponseMessage() + ")");
                            }else{
                                dbLogWriter.warning(context, "isConnectionToBackendAvailable = FALSE (" + connection.getResponseCode() + " - " + connection.getResponseMessage() + ")");
                            }

                            return (connection.getResponseCode() == 200);
                        }

                    } catch (IOException e) {
                        //Log.e(TAG, "CaughtError checking internet connection", e);
                        dbLogWriter.error(context, e,"CaughtError checking internet connection: IOException - " + Arrays.toString(e.getStackTrace()), ErrorCodesEnum.ERROR_90004, false);
                    }
                    return false;
                }
            }.execute().get();

        } catch (InterruptedException | ExecutionException e) {
            dbLogWriter.error(context, e,"InterruptedException | ExecutionException: " + Arrays.toString(e.getStackTrace()), ErrorCodesEnum.ERROR_00101, false);
        }

        return false;
    }





    public static String getResponseCode() {
        return responseCode;
    }

    public static void setResponseCode(String responseCode) {
        ConnectionHelper.responseCode = responseCode;
    }

    public static String getURL(int env){

        String url = "";

        switch (env) {
            case 1:
                url = URLEnum.URL_DEV.getEnumString();
                break;
            case 2:
                url = URLEnum.URL_TEST.getEnumString();
                break;
            case 3:
                url = URLEnum.URL_PROD.getEnumString();
                break;
            default:
                //TODO set default
                break;
        }
        return url;
    }

    public static String getUserAndPassword(int env){

        String username = "";
        String password = "";
        String userAndPassword = "";
        String encoding = "";

        switch (env) {
            case 1:
                username = ConnectionEnum.BASIC_AUTH_DEV_USER.getEnumString();
                password = ConnectionEnum.BASIC_AUTH_DEV_PASSWORD.getEnumString();
                break;
            case 2:
                username = ConnectionEnum.BASIC_AUTH_TEST_USER.getEnumString();
                password = ConnectionEnum.BASIC_AUTH_TEST_PASSWORD.getEnumString();
                break;
            case 3:
                username = ConnectionEnum.BASIC_AUTH_PROD_USER.getEnumString();
                password = ConnectionEnum.BASIC_AUTH_PROD_PASSWORD.getEnumString();
                break;
            default:
                //TODO set default
                break;
        }


        userAndPassword = username + ":" + password;
        encoding = Base64.encodeToString(userAndPassword.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);

        return encoding;

    }
}
