package com.example.pint.enums;

public enum ErrorCodesEnum {

    //Unknow error
    ERROR_00001 ("00001 - Unknown error"),
    ERROR_00002 ("00002 - Uncaught Exception Handler"),

    //Default Errors
    ERROR_00101 ("00101 - Verification Error"),
    ERROR_00102 ("00102 - Parse Excpetion"),
    ERROR_00103 ("00103 - Interupted Excpetion"),

    //Room DB Errors
    ERROR_10000 ("10000 - Unknown Room DB error"),
    ERROR_10001 ("10001 - Error while getting or setting Data from Room DB with Model"),
    ERROR_10002 ("10002 - Error while getting Data from Room DB with Model"),

    //Custom Function Errors
    ERROR_90001 ("90001 - "),
    ERROR_90002 ("90002 - Error while scanning Barcode"),
    ERROR_90003 ("90003 - Error while handlign Dates"),
    ERROR_90004 ("90004 - Error while checking internet connection"),
    ERROR_90005 ("90005 - "),
    ERROR_90006 ("90006 - "),
    ERROR_90007 ("90007 - "),
    ERROR_90008 ("90008 - Error while restarting the App"),
    ERROR_90009 ("90009 - "),
    ERROR_90010 ("90010 - ");


    private String enumString;

    public String getEnumString() {
        return String.valueOf(enumString);
    }

    ErrorCodesEnum(String enumString) {
        this.enumString = enumString;
    }

}
