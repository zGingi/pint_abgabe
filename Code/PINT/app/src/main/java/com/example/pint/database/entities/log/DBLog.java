package com.example.pint.database.entities.log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "dblog")
public class DBLog {

    @PrimaryKey(autoGenerate = true)
    Integer id;

    @ColumnInfo(name = "type")
    String type;

    @ColumnInfo(name = "text")
    String text;

    @ColumnInfo(name = "datum")
    String datum;

    @ColumnInfo(name = "query")
    String query;

    @ColumnInfo(name = "user")
    String user;

    @ColumnInfo(name = "uploaded")
    Integer uploaded;


    public DBLog(){

    }

    @Ignore
    public DBLog(String type, String text, String datum, String user) {
        this.type = type;
        this.text = text;
        this.datum = datum;
        this.user = user;
    }

    @Ignore
    public DBLog(String type, String text, String datum, String query, String user) {
        this.type = type;
        this.text = text;
        this.datum = datum;
        this.query = query;
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getUploaded() {
        return uploaded;
    }

    public void setUploaded(Integer uploaded) {
        this.uploaded = uploaded;
    }

    @Override
    public String toString() {
        return "DBLog{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", text='" + text + '\'' +
                ", datum='" + datum + '\'' +
                ", query='" + query + '\'' +
                ", user='" + user + '\'' +
                ", uploaded='" + uploaded + '\'' +
                '}';
    }

}