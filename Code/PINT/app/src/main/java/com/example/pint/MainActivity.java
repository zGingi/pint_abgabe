package com.example.pint;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;

import com.example.pint.barcode.BarcodeCaptureActivity;
import com.example.pint.database.AppDatabase;
import com.example.pint.database.entities.User;
import com.example.pint.database.model.UserModel;
import com.example.pint.database.model.log.DBLogModel;
import com.example.pint.database.model.settings.SettingsModel;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.DBLogWriter;
import com.example.pint.util.LanguageHelper;
import com.example.pint.util.LogoutHelper;
import com.example.pint.util.SharedPreferenceHelper;
import com.example.pint.util.UnCaughtException;
import com.example.pint.util.ViewUtils;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.ViewModelProviders;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int RC_BARCODE_CAPTURE = 9001;
    private static final String TAG = MainActivity.class.getName();

    Button erfassenButton;
    Button punkteButton;

    UserModel userModel;
    SettingsModel settingsModel;
    DBLogModel dbLogModel;

    User user;

    String activeUser;
    String nameUser;
    String vornameUser;
    String valueBarcodeUser;
    Barcode barcode;

    DBLogWriter dBLogWriter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LanguageHelper.updateLanguage(MainActivity.this);


        if (!ViewUtils.isLandscapeEnabled(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        dBLogWriter = new DBLogWriter();
        dBLogWriter.info(MainActivity.this, "////////////////////////////////////////////////////");
        dBLogWriter.info(MainActivity.this, "//STARTET MAIN ACTIVITY");
        dBLogWriter.info(MainActivity.this, "//Source Call: " + getIntent().getStringExtra("Source"));
        dBLogWriter.info(MainActivity.this, "////////////////////////////////////////////////////");

        //@Info:    Prevent Keyboard from appearing on activity start
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //@Info:    Content View
        setContentView(R.layout.activity_main);

        //@Info:    Set Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //@Info:    Override Uncaught exception handler
        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(MainActivity.this));

        //Instantiate Modells
        userModel = ViewModelProviders.of(MainActivity.this).get(UserModel.class);
        settingsModel = ViewModelProviders.of(MainActivity.this).get(SettingsModel.class);
        dbLogModel = ViewModelProviders.of(MainActivity.this).get(DBLogModel.class);

        //@INFO
        //Check if flag to exit was send
        if (getIntent().getBooleanExtra("EXIT", false)) {
            //@DEBUG
            dBLogWriter.info(MainActivity.this, "Trying to exit the app");
            finishAndRemoveTask();
            System.exit(0);

        }

        //@Info:    Initialize Buttons
        erfassenButton = (Button) findViewById(R.id.button_erfassen_starten);
        punkteButton = (Button) findViewById(R.id.button_punkte_einloesen);

        //@Info:    Start new Scanning activity on button press
        //          All logic in the following function: onActivityResult()
        erfassenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BarcodeCaptureActivity.class);
                intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
                intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
                startActivityForResult(intent, RC_BARCODE_CAPTURE);
            }
        });

        //@Info:    Continue to Punkte
        punkteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, PunkteActivity.class);
                intent.putExtra("Source", "MainActivity4");
                startActivity(intent);

            }
        });

        //Hanlde Drawer and navigation
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_erfassung);
        navigationView.setNavigationItemSelectedListener(this);

        // Show logged in User in Navigation
        View headerView = navigationView.getHeaderView(0);
        TextView loggedInUser = (TextView) headerView.findViewById(R.id.textLoggedInUser);
        loggedInUser.setText(SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_USER.toString(), "#########"));

        TextView activeModuleTextview = (TextView) headerView.findViewById(R.id.textActiveModule);
        activeModuleTextview.setText(SharedPreferenceHelper.getSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_MODULE.toString(), "<Kein Aktives Modul>"));

    }


    @Override
    protected void onStart() {
        super.onStart();

        //@INFO: Disable Auto-Fill for Oreo
        disableAutoFill();

        dBLogWriter = new DBLogWriter();
        settingsModel = ViewModelProviders.of(MainActivity.this).get(SettingsModel.class);
        userModel = ViewModelProviders.of(MainActivity.this).get(UserModel.class);

    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutoFill() {
        //@INFO:
        //Disable Auto-Fill for Oreo
        //Source: https://stackoverflow.com/questions/45731372/disabling-android-o-auto-fill-service-for-an-application
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
        }

    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //@INFO 06.12.2017 - MAS02
            // Starts logout process from LogoutHelper
            LogoutHelper.logout(this, settingsModel);
        }
    }

    /**
     * Called when an activity you launched exits, giving you the requestCode
     * you started it with, the resultCode it returned, and any additional
     * data from it.  The <var>resultCode</var> will be
     * {@link } if the activity explicitly returned that,
     * didn't return any result, or crashed during its operation.
     * <p/>
     * <p>You will receive this call immediately before onResume() when your
     * activity is re-starting.
     * <p/>
     *
     * @param requestCode The integer request code originally supplied to
     *                    startActivityForResult(), allowing you to identify who this
     *                    result came from.
     * @param resultCode  The integer result code returned by the child activity
     *                    through its setResult().
     * @param data        An Intent, which can return result data to the caller
     *                    (various data can be attached to Intent "extras").
     * @see #startActivityForResult
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    barcode = data.getParcelableExtra(BarcodeCaptureActivity.BarcodeObject);
                    valueBarcodeUser = barcode.displayValue;

                    final ProgressDialog pd = new ProgressDialog(MainActivity.this);
                    pd.setMessage(getResources().getString(R.string.bitte_warten) + ". " + getResources().getString(R.string.daten_werden_kontrolliert) + "...");
                    pd.show();

                    //@INFO
                    //Check if Barcode/QR Code is empty
                    if (valueBarcodeUser != null && !valueBarcodeUser.equals("")) {

                        SharedPreferenceHelper.setSharedPreferenceString(this, SharedPreferencesEnum.ACTIVE_MODULE.toString(), valueBarcodeUser);

                        Toast.makeText(this, "Erfolgreich an folgendem Modul angemeldet: " + valueBarcodeUser, Toast.LENGTH_SHORT).show();

                        Intent intentErfassen = new Intent(MainActivity.this, ErfassenActivity.class);
                        intentErfassen.putExtra("Source", "MainActivity1");
                        startActivity(intentErfassen);

                    } else {
                        Toast.makeText(this, "Ungültiger Barcode read: " + valueBarcodeUser, Toast.LENGTH_LONG).show();
                        dBLogWriter.debug(MainActivity.this, "Ungültiger Barcode read: " + valueBarcodeUser);
                    }
                    pd.dismiss();

                } else {
                    //statusMessage.setText(R.string.barcode_failure);
                    Toast.makeText(this, "No barcode captured, intent data is null", Toast.LENGTH_LONG).show();
                    dBLogWriter.debug(MainActivity.this, "No barcode captured, intent data is null");
                }
            } else {
                //Handle error
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_erfassung) {

        } else if (id == R.id.nav_einstellungen) {

            Intent intent = new Intent(MainActivity.this, EinstellungenActivity.class);
            intent.putExtra("Source", "MainActivity2");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (id == R.id.nav_info) {

            Intent intent = new Intent(MainActivity.this, AppInfoActivity.class);
            intent.putExtra("Source", "MainActivity3");
            startActivity(intent);

        } else if (id == R.id.nav_log) {

            Intent intent = new Intent(MainActivity.this, LogOverviewActivity.class);
            intent.putExtra("Source", "MainActivity6");
            startActivity(intent);

        }else if (id == R.id.nav_abmelden) {

            //@INFO
            // Starts logout process from LogoutHelper
            LogoutHelper.logout(this, settingsModel);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}