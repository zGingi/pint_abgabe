package com.example.pint;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.pint.adapter.ListViewAdapter_Log;
import com.example.pint.database.entities.log.DBLog;
import com.example.pint.database.model.log.DBLogModel;
import com.example.pint.enums.SharedPreferencesEnum;
import com.example.pint.util.ErrorHelper;
import com.example.pint.util.SharedPreferenceHelper;
import com.example.pint.util.UnCaughtException;
import com.example.pint.util.ViewUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class LogExplorerActivity extends AppCompatActivity {

    String TAG = LogExplorerActivity.class.getName();

    Spinner spinnerDBTables;

    DBLogModel dbLogModel;

    List<DBLog> dbLogs;
    List<DBLog> all_dbLogs;

    EditText inputSearch;

    List<DBLog> listviewContent;
    List<DBLog> listviewAll;
    List<DBLog> listviewInfo;
    List<DBLog> listviewDebug;
    List<DBLog> listviewWarning;
    List<DBLog> listviewError;
    List<DBLog> listviewCustom;
    List<DBLog> listviewArchive;
    List<DBLog> listviewSummary;
    ArrayAdapter<DBLog> adapterList;

    String[] dbtables;

    int activeView = 0;

    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!ViewUtils.isLandscapeEnabled(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout._admin_activity_log_explorer);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(LogExplorerActivity.this));

        dbLogModel = ViewModelProviders.of(LogExplorerActivity.this).get(DBLogModel.class);

        try {
            new AsyncTask<Void, Void, Void>() {

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    pd = new ProgressDialog(LogExplorerActivity.this);
                    pd.setMessage("Loading log content.");
                    pd.show();
                }

                @Override
                protected Void doInBackground(Void... params) {

                    if(null != getIntent().getStringExtra("date") && !getIntent().getStringExtra("date").equals("")){
                        dbLogs = dbLogModel.getLogsForDate(getIntent().getStringExtra("date"), 0);
                        all_dbLogs = dbLogs; //allDbLogs used for file System epxort
                    }else{
                        dbLogs = dbLogModel.getAllDBLog(true, 2000);
                        all_dbLogs = dbLogModel.getAllDBLog(false, 0); //allDbLogs used for file System epxort
                    }


                    return null;

                }

                @Override
                protected void onPostExecute(Void result) {
                    // do UI work here
                    if (pd.isShowing()) {
                        pd.dismiss();
                    }
                }
            }.execute().get();
        } catch (InterruptedException | ExecutionException e) {
           new ErrorHelper().showErrorAlert_Exception(LogExplorerActivity.this, e); //e.printStackTrace();
        }

        //@DEBUG
        Log.d(TAG, "Log Size: " + dbLogs.size() + " Lines");

        listviewContent = new ArrayList<>();
        listviewAll = new ArrayList<>();
        listviewInfo = new ArrayList<>();
        listviewDebug = new ArrayList<>();
        listviewWarning = new ArrayList<>();
        listviewError = new ArrayList<>();
        listviewCustom = new ArrayList<>();
        listviewArchive = new ArrayList<>();
        listviewSummary = new ArrayList<>();

        for (DBLog l : dbLogs) {
            String log = l.getDatum() + "- (" + l.getType() + ")\nUser: " + SharedPreferenceHelper.getSharedPreferenceString(LogExplorerActivity.this, SharedPreferencesEnum.ACTIVE_USER.toString(), "") + "\nLog: \n" + l.getText() + "\n";

            switch (l.getType()) {
                case "INFO":
                    listviewInfo.add(l);
                    listviewCustom.add(l);
                    listviewAll.add(l);
                    break;
                case "DEBUG":
                    listviewDebug.add(l);
                    listviewAll.add(l);
                    break;
                case "WARNING":
                    listviewWarning.add(l);
                    listviewCustom.add(l);
                    listviewAll.add(l);
                    break;
                case "ERROR":
                    listviewError.add(l);
                    listviewCustom.add(l);
                    listviewAll.add(l);
                    break;
                case "ARCHIVE":
                    listviewArchive.add(l);
                    listviewAll.add(l);
                    break;
                case "SUMMARY":
                    listviewSummary.add(l);
                    listviewAll.add(l);
                    break;
                default:
                    listviewAll.add(l);
            }
        }

        dbtables = new String [] {
                "ALL (" + listviewAll.size() + ")",
                "INFO (" + listviewInfo.size() + ")",
                "DEBUG (" + listviewDebug.size() + ")",
                "WARNING (" + listviewWarning.size() + ")",
                "ERROR (" + listviewError.size() + ")",
                "INFO, WARNING and ERROR (" + listviewCustom.size() + ")",
                "ARCHIVE (" + listviewArchive.size() + ")",
                "SUMMARY (" + listviewSummary.size() + ")"
        };

        adapterList = new ListViewAdapter_Log(LogExplorerActivity.this, listviewContent);
        ListView mlistView = (ListView) findViewById(R.id.listview_log_content);
        mlistView.setAdapter(adapterList);

        adapterList.notifyDataSetChanged();

        spinnerDBTables = (Spinner) findViewById(R.id.spinner_db_tables);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<String> adapter = new ArrayAdapter<>(LogExplorerActivity.this,
                R.layout.spinner_item_single_text_line, dbtables);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinnerDBTables.setAdapter(adapter);

        spinnerDBTables.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //@DEBUG
                Log.d(TAG, "Clicked on Item number: " + position);

                listviewContent.clear();

                switch (position) {
                    case 0:
                        listviewContent.addAll(listviewAll);
                        activeView =0;
                        break;
                    case 1:
                        listviewContent.addAll(listviewInfo);
                        activeView =1;
                        break;
                    case 2:
                        listviewContent.addAll(listviewDebug);
                        activeView =2;
                        break;
                    case 3:
                        listviewContent.addAll(listviewWarning);
                        activeView =3;
                        break;
                    case 4:
                        listviewContent.addAll(listviewError);
                        activeView =4;
                        break;
                    case 5:
                        listviewContent.addAll(listviewCustom);
                        activeView =5;
                        break;
                    case 6:
                        listviewContent.addAll(listviewArchive);
                        activeView =6;
                        break;
                    case 7:
                        listviewContent.addAll(listviewSummary);
                        activeView =7;
                        break;
                    default:
                        listviewContent.addAll(listviewAll);
                        activeView =0;

                }

                if (listviewContent.size() == 0) {
                    listviewContent.add(new DBLog("", dbtables[position] + " - Keine Daten in der DB", "", "" ));
                }

                adapterList.notifyDataSetChanged();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        inputSearch = (EditText) findViewById(R.id.inputSearch);
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {


                listviewContent.clear();

                switch (activeView){
                    case 0:
                        listviewContent.addAll(listviewAll);
                        break;
                    case 1:
                        listviewContent.addAll(listviewInfo);
                        break;
                    case 2:
                        listviewContent.addAll(listviewDebug);
                        break;
                    case 3:
                        listviewContent.addAll(listviewWarning);
                        break;
                    case 4:
                        listviewContent.addAll(listviewError);
                        break;
                    case 5:
                        listviewContent.addAll(listviewCustom);
                        break;
                    case 6:
                        listviewContent.addAll(listviewArchive);
                        break;
                    case 7:
                        listviewContent.addAll(listviewSummary);
                        break;
                    default:
                        listviewContent.addAll(listviewAll);
                }


                if (cs != null || cs.length() != 0) {

                    List<DBLog> filteredList = new ArrayList<DBLog>();
                    for (DBLog l : listviewContent) {
                        if (l.getText().contains(cs))
                            filteredList.add(l);
                    }

                    listviewContent.clear();
                    listviewContent.addAll(filteredList);

                    adapterList.notifyDataSetChanged();

                }else{

                    listviewContent.clear();

                    switch (activeView){
                        case 0:
                            listviewContent.addAll(listviewAll);
                            break;
                        case 1:
                            listviewContent.addAll(listviewInfo);
                            break;
                        case 2:
                            listviewContent.addAll(listviewDebug);
                            break;
                        case 3:
                            listviewContent.addAll(listviewWarning);
                            break;
                        case 4:
                            listviewContent.addAll(listviewError);
                            break;
                        case 5:
                            listviewContent.addAll(listviewCustom);
                            break;
                        case 6:
                            listviewContent.addAll(listviewArchive);
                            break;
                        default:
                            listviewContent.addAll(listviewAll);
                    }

                    adapterList.notifyDataSetChanged();

                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

            @Override
            public void afterTextChanged(Editable arg0) {}
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
